// package getfromeditor allows capturing or editing of data using
// an external editor such as vim
package getfromeditor

import (
    "errors"
    "io/ioutil"
    "os"
    "os/exec"
    "runtime"
)


const (
    ErrTmp = "cannot access tmpfile"
    ErrDetermineEditor = "cannot determine editor"
)

// GetFromEditor opens data in editor and returns the edited data.
// If data is nil, the editor opens with an empty file
// If editor is the empty string, GetFromEditor tries to open a default
// Editor using methods common to the system
func GetFromEditor(editor string, data []byte) ([]byte, error) {

    // make tmp file
    tempF, err := ioutil.TempFile("", "goGFEtmp*.txt")
    if err != nil {
        return nil, errors.New(ErrTmp)
    }
    tempFn := tempF.Name()

    // write data to tmp-file
    if len(data) > 0 {
        if err := ioutil.WriteFile(tempFn, data, 0644); err != nil {
            return nil, errors.New(ErrTmp)
        }
    }

    // close temp file
    tempF.Close()

    // get editor
    if editor == "" {
        editor, err = GetDefaultEditor()
        if err != nil {
            return nil, err
        }
    }

    // open tmpfile in editor
    cmd := exec.Command(editor, tempFn)
    cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
    cmd.Run()

    //read & return
    rdata, err := ioutil.ReadFile(tempFn)
    if err != nil {
        return nil, errors.New(ErrTmp)
    }

    // delete tmp file
    if err := os.Remove(tempFn); err != nil {
        return nil, errors.New(ErrTmp)
    }
    return rdata, nil

}

// GetDefaultEditor returns the path of the systems default editors
// Executable, or error if it cannot be determined
func GetDefaultEditor() (string, error) {

    // windows
    // notepad.exe is the only really consistent editor
    if runtime.GOOS == "windows" {
        windir := os.Getenv("windir")
        if windir == "" {
            return "", errors.New(ErrDetermineEditor)
        }
        fname := windir + "/system32/notepad.exe"
        if _, err := os.Stat(fname); err != nil {
            return "", errors.New(ErrDetermineEditor)
        }
        return fname, nil
    }

    // POSIX
    editor := os.Getenv("EDITOR")
    if editor == "" {
        editor = os.Getenv("VISUAL")
    }
    if editor == "" {
        return "", errors.New(ErrDetermineEditor)
    }
    return editor, nil
}
