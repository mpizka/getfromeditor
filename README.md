# getfromeditor

Small package to edit data in external editorspackage getfromeditor // import "gitlab.com/mpizka/getfromeditor"

# go doc #

```go
// package getfromeditor allows capturing or editing of data using an external
// editor such as vim

// CONSTANTS

const (
	ErrTmp             = "cannot access tmpfile"
	ErrDetermineEditor = "cannot determine editor"
)

// FUNCTIONS

func GetDefaultEditor() (string, error)
//  GetDefaultEditor returns the path of the systems default editors Executable,
//  or error if it cannot be determined

func GetFromEditor(editor string, data []byte) ([]byte, error)
//    GetFromEditor opens data in editor and returns the edited data. If data is
//    nil, the editor opens with an empty file If editor is the empty string,
//    GetFromEditor tries to open a default Editor using methods common to the
//    system
```
